import React from 'react'

export default function TableComponent({ getPersonData }) {

    return (
        <div>
  
            <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-4/5 mx-auto text-sm text-center text-gray-500 dark:text-gray-400 overflow-hidden rounded-md">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-6 py-3">
                                ID
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Email
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Username
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Age
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        {getPersonData.map((item) => (
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600">
                                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {item.id}
                                </th>
                                <td class="px-6 py-4">
                                    {item.email}
                                </td>
                                <td class="px-6 py-4">
                                    {item.username}
                                </td>
                                <td class="px-6 py-4">
                                    {item.age}
                                </td>
                            </tr>

                        ))}


                    </tbody>
                </table>
            </div>

        </div>
    )
}
