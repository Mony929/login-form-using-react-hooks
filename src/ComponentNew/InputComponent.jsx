import React, { useState } from 'react'
import TableComponent from './TableComponent'

export default function InputComponent({ getDefaultPerson, setData }) {

    const [person, setPerson] = useState({});

    const handleInputChange = (event) => {
        setPerson({ ...person, [event.target.name]: event.target.value });
    }

    const handleSubmit = (event) => {
        setData([...getDefaultPerson, { id: getDefaultPerson.length + 1, ...person }]);
        event.preventDefault();
    }


    return (
        <div>
            {/* <div className='justify-center max-w-5xl text-center mx-auto bg-red-200'>
                <label className="text-start">Email</label> <br />
                <div className="">

                    <input onChange={handleInputChange} type="email" name="email" id="" className='w-80 border border-gray-400 mb-2  rounded-md ' /> <br />
                </div>

                <div className=''>
                    <label htmlFor="" className='text-start'>Username</label> <br />
                    <input onChange={handleInputChange} type="text" name="username" id="" className='w-80 border border-gray-400 mb-2  rounded-md' /> <br />

                </div>
                <div className=''>
                    <label htmlFor="" className='mb-2'>Age</label> <br />
                    <input onChange={handleInputChange} type="number" name="age" id="" className='w-80 border border-gray-400 mb-2  rounded-md' /> <br />

                </div>
                <div className=''>
                    <button onClick={handleSubmit} type='submit' className='py-1 px-5 bg-red-300 rounded-xl'>Submit</button>

                </div>

            </div> */}

            <div class="w-full mx-auto mb-10 mt-5 max-w-sm p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
                <form class="space-y-6" action="#">
                    <h5 class="text-xl font-medium text-gray-900 dark:text-white">User Information</h5>
                    <div>
                        <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your email</label>
                        <input onChange={handleInputChange} type="email" name="email" id="email" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="name@company.com" required />
                    </div>
                    <div>
                        <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                        <input onChange={handleInputChange} type="text" name="username" id="username" placeholder="mony" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required />
                    </div>
                    <div>
                        <label for="age" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Age</label>
                        <input onChange={handleInputChange} type="number" name="age" placeholder="20" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required />
                    </div>
                    <button onClick={handleSubmit} type="submit" class="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>

                </form>
            </div>


            <TableComponent getPersonData={getDefaultPerson} />
        </div>

    )
}
