import { useState } from 'react';
import './App.css';

import InputComponent from './ComponentNew/InputComponent';

function App() {


  const [person, setPerson] = useState([
    
    {
      id: 1,
      email : "mony29@gmail.com",
      username : "mony",
      age : 10
    },
    {
      id: 2,
      email : "kimheng@gmail.com",
      username : "kimheng",
      age : 20
    },
    {
      id: 3,
      email : "dara@gmail.com",
      username : "dara",
      age : 25
    },
  ])


  return (
    // <HomeComponent></HomeComponent>
    // <HooksComponent ></HooksComponent>
    <InputComponent getDefaultPerson={person} setData={setPerson}/>

    

  );
}

export default App;
